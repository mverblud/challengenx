const getAll = require('../repositories/user/userCase/getClients/getClients');
const createClient = require('../repositories/user/userCase/createClient/createClient');
const updateCity = require('../repositories/user/userCase/updateCity/updateCity');
const deleteCity = require('../repositories/user/userCase/deleteCity/deleteCity');

module.exports = {
    getAll,
    createClient,
    updateCity,
    deleteCity,
}