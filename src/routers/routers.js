const { Router } = require('express');
const router = new Router();

const { getCities, getCity } = require('../repositories/user/userCase/getCities/getCities');
const { createCity } = require('../repositories/user/userCase/createCity/createCity');
const { updateCity } = require('../repositories/user/userCase/updateCity/updateCity');
const { deleteCity } = require('../repositories/user/userCase/deleteCity/deleteCity');

const { getItineraries, getItinerary } = require('../repositories/user/userCase/getItineraries/getItineraries');
const { createItinerary } = require('../repositories/user/userCase/createItinerary/createItinerary');
const { updateItinerary } = require('../repositories/user/userCase/updateItinerary/updateItinerary');
const { deleteItinerary } = require('../repositories/user/userCase/deleteItinerary/deleteItinerary');

router.get('/cities', getCities);
router.get('/cities/:id', getCity);
router.post('/cities', createCity);
router.put('/cities/:id', updateCity);
router.delete('/cities/:id', deleteCity);

router.get('/itineraries', getItineraries);
router.get('/itineraries/:id', getItinerary);
router.post('/itineraries', createItinerary);
router.put('/itineraries/:id', updateItinerary);
router.delete('/itineraries/:id', deleteItinerary);

module.exports = router;