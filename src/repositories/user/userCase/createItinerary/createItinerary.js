const { response } = require('express');
const itineraryRepository = require('../../../../repositories/itineraryRepository');

const createItinerary = async (req, res = response) => {

    try {
        await itineraryRepository.save(req.body);

        return res.status(201).json({
            message: 'el Itinerario se creo correctamente',
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    createItinerary,
}