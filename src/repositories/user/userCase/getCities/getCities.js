const { response } = require('express');
const cityRepository = require('../../../../repositories/cityRepository');

const getCities = async (req, res = response) => {

    try {
        const cities = await cityRepository.getAll();
        const count = await cityRepository.count();

        if (!cities) {
            return res.status(401).json({
                message: 'Not found',
            })
        }

        res.status(200).json({
            message: 'cities',
            response: cities,
            total: count
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

const getCity = async (req, res = response) => {
    const id = req.params.id;

    try {

        const city  = await cityRepository.getOne(id);

        if (!city) {
            return res.status(401).json({
                message: 'Not found',
            })
        }

        res.status(200).json({
            message: 'city',
            response: city,
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getCities,
    getCity,
}