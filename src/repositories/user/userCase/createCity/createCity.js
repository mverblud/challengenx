const { response } = require('express');
const cityRepository = require('../../../../repositories/cityRepository');

const createCity = async (req, res = response) => {

    try {
        await cityRepository.save(req.body);

        return res.status(201).json({
            message: 'La ciudad se creo correctamente',
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    createCity,
}