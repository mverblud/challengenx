const { response } = require('express');
const itineraryRepository = require('../../../../repositories/itineraryRepository');

const getItineraries = async (req, res = response) => {

    try {
        const itineraries = await itineraryRepository.getAll();
        const count = await itineraryRepository.count();

        if (!itineraries) {
            return res.status(401).json({
                message: 'Not found',
            })
        }

        res.status(200).json({
            message: 'itineraries',
            response: itineraries,
            total: count
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

const getItinerary = async (req, res = response) => {
    const id = req.params.id;

    try {

        const itinerary  = await itineraryRepository.getOne(id);

        if (!itinerary) {
            return res.status(401).json({
                message: 'Not found',
            })
        }

        res.status(200).json({
            message: 'itinerary',
            response: itinerary,
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getItineraries,
    getItinerary,
}