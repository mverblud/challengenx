const Itinerary = require('../models/itineraryModel');

const getAll = async () => await Itinerary.find();
const getOne = async (id) => await Itinerary.findById(id);
const count = async () => await Itinerary.count();

const deleteOne = async (id)  => await Itinerary.findByIdAndRemove(id);
const updateOne = async (id, body)  => await Itinerary.findByIdAndUpdate(id, body);

const save = async (body) => {

    const itinerary = new Itinerary({
        name: body.name,
        img: body.img,
        price: body.price,
        duration: body.duration,
        likes: body.likes,
        hashtags: body.hashtags,
        comments: body.comments,
        activities: body.activities,
    })

    await itinerary.save();

    return itinerary;
}

module.exports = {
    getAll,
    getOne,
    count,
    save,
    deleteOne,
    updateOne,
};