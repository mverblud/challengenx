const mongoose = require('mongoose');
const { Schema } = mongoose;

const itinerarySchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true, },
    img: { type: String, required: true },
    price: { type: Number, required: true, min: 1, max: 5 },
    duration: { type: Number, required: true, min: 1, max: 5 },
    likes: { type: Number, default: 0, },
    hashtags: { type: String, },
    comments: [{ type: String, }],
    activities: [{ type: String, }],
});

module.exports = mongoose.model('itinerary', itinerarySchema);