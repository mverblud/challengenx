const getAll = require('../repositories/user/userCase/getItineraries/getItineraries');
const createItinerary = require('../repositories/user/userCase/createItinerary/createItinerary');
const updateItinerary = require('../repositories/user/userCase/updateItinerary/updateItinerary');
const deleteItinerary = require('../repositories/user/userCase/deleteItinerary/deleteItinerary');

module.exports = {
    getAll,
    createItinerary,
    updateItinerary,
    deleteItinerary,
}