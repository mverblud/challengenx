const express = require('express');
require('dotenv').config();
const mongoose = require('mongoose')

const cors = require('cors');

const app = express()
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get('/cities', (req, res) => {
    res.send('Hello World!')
})

app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`)
})

const routers = require('./src/routers/routers');

app.use('/api/v1', routers);

mongoose.connect(process.env.MONGODB, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Conexión a MongoDB establecida'))
    .catch(err => console.log(err))